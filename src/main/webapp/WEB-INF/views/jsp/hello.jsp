<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
  
<html lang="en">
<head>
  <title>Insurance Premium</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2> Insurance Details</h2>
  <p>Please Enter Personal Details :</p>
  <form:form class="form-inline" method="post" action="save" >
    
    
    <div class="form-group">
      <label for="User Name">User Name:</label>
      <input type="text" class="form-control" id="name" placeholder="User Name" name="name" path="name"/>
    </div>
    
    
    <div class="form-group">
      <label for="gender">Gender:</label>
      <input type="text" class="form-control" id="gender" placeholder="gender" name="gender" path="gender"/>
    </div>
    
    <div class="form-group">
      <label for="age">Age:</label>
      <input type="text" class="form-control" id="age" placeholder="age" name="age" path="age"/>
    </div>
    <div class="form-group">
      <label for="Hypertension">Hypertension:</label>
      <input type="text" class="form-control" id="Hypertension" placeholder="Hypertension" name="Hypertension" path="Hypertension"/>
    </div>
    <div class="form-group">
      <label for="pressure">pressure:</label>
      <input type="text" class="form-control" id="pressure" placeholder="pressure" name="pressure" path="pressure"/>
    </div>
    <div class="form-group">
      <label for="">sugar:</label>
      <input type="text" class="form-control" id="sugar" placeholder="sugar" name="sugar" path="sugar"/>
    </div>
    <div class="form-group">
      <label for="Overweight">Overweight:</label>
      <input type="text" class="form-control" id="Overweight" placeholder="Overweight" name="Overweight" path="Overweight"/>
    </div>
    <div class="form-group">
      <label for="Smoking">Smoking:</label>
      <input type="text" class="form-control" id="Smoking" placeholder="Smoking" name="Smoking" path="Smoking"/>
    </div>
    <div class="form-group">
      <label for="Alcohol">Alcohol:</label>
      <input type="text" class="form-control" id="Alcohol" placeholder="Alcohol" name="Alcohol" path="Alcohol"/>
    </div>
    <div class="form-group">
      <label for="exercise">exercise:</label>
      <input type="text" class="form-control" id="exercise" placeholder="exercise" name="exercise" path="exercise"/>
    </div>
    <div class="form-group">
      <label for="Drugs">Drugs:</label>
      <input type="text" class="form-control" id="Drugs" placeholder="Drugs" name="Drugs" path="Drugs"/>
    </div>
    
        
    
    <br><br><br>
    <button type="submit" class="btn btn-default">Submit</button>
  </form:form>
</div>

</body>
</html>
